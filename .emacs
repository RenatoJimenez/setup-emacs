;; Makes *scratch* empty.
(setq initial-scratch-message "")

;; Removes *scratch* from buffer after the mode has been set.
(defun remove-scratch-buffer ()
  (if (get-buffer "*scratch*")
      (kill-buffer "*scratch*")))
(add-hook 'after-change-major-mode-hook 'remove-scratch-buffer)

;; Removes *messages* from the buffer.
(setq-default message-log-max nil)
(kill-buffer "*Messages*")

;; Removes *Completions* from buffer after you've opened a file.
(add-hook 'minibuffer-exit-hook
      '(lambda ()
         (let ((buffer "*Completions*"))
           (and (get-buffer buffer)
                (kill-buffer buffer)))))

;; Don't show *Buffer list* when opening multiple files at the same time.
(setq inhibit-startup-buffer-menu t)

;; Show only one active window when opening multiple files at the same time.
(add-hook 'window-setup-hook 'delete-other-windows)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (deeper-blue)))
 '(indent-tabs-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-modified ((t (:foreground "red"))))
 '(font-lock-comment-face ((t (:foreground "dim gray")))))

; load all .el files from this directory
(add-to-list 'load-path "~/.emacs.d/lisp/")

; no start up buffers
(setq inhibit-startup-message t)

; compile will auto-scroll to follow output
(setq compilation-scroll-output 1)

; replace selected text
(delete-selection-mode 1)

; disable toolbar by default
(tool-bar-mode -1)

; enable CUA (copy, cut, paste with C-c, C-w, C-v) 
(cua-mode 1)

; always save our current buffers
(setq desktop-dirname              "~/.emacs.d/"
      desktop-base-file-name       "emacs.desktop"
      desktop-base-lock-name       "lock"
      desktop-path                 (list desktop-dirname)
      desktop-save                 t
      desktop-files-not-to-save    "^$"
      desktop-load-locked-desktop  nil)
	  
;; Automatically save and restore sessions
(setq desktop-dirname             "~/.emacs.d/"
      desktop-base-file-name      "emacs.desktop"
      desktop-base-lock-name      "lock"
      desktop-path                (list desktop-dirname)
      desktop-save                t
      desktop-files-not-to-save   "^$" ;reload tramp paths
      desktop-load-locked-desktop nil)
(desktop-save-mode 1)

; enable todo/note color tags
(require 'fic-mode)
(add-hook 'c++-mode-hook 'fic-mode)
(add-hook 'emacs-lisp-mode-hook 'fic-mode)

; enable modeline position
; (shows position of character in line and the line number)
(require 'modeline-posn)
(column-number-mode 1)
(size-indication-mode 1)
(setq modelinepos-column-limit 80)

; copy a default style and add our own properties
(c-add-style "waves-style"
  '("cc-mode"
  (setq c-basic-offset    4)
  (setq tab-width         4)
  (setq indent-tabs-mode  nil)
  (c-echo-syntactic-information-p . t)
  (c-comment-only-line-offset . (0 . 0))
  (c-offsets-alist . (
    (c                     . c-lineup-C-comments)
    (statement-case-open   . 0)
    (case-label            . /)
    (substatement-open     . 0)
    (innamespace           . 0)
    (inextern-lang         . 0)
    (inline-open           . 0)
    (label                 . *)
    ))
  )
)

; set our edit default style to waves-style and add
; a tab offset of 4 spaces
(setq c-default-style "waves-style")

; enable electric mode (auto-indent new lines)
(electric-indent-mode 1)
                
; add a keyboard shortcut to split buffer
; into a pane of 3 windows (top left, top right, bottom)
(defun split-pane-three ()
  "Split the pane into three windows"
  (interactive)
  (split-window-below 44)
  (split-window-right)
)
(global-set-key (kbd "C-x 4") 'split-pane-three)

; set our back up directory
(setq backup-directory-alist `(("." . "~/.saves")))

; fast-build for Waves:  ALT - b
(setq compile-command "n:/build.bat")
(setq compilation-read-command nil)
(global-set-key (kbd "M-b") 'compile)

; delete window: CTRL - SHIFT - S
(global-set-key (kbd "C-x w") 'delete-window)

; transpose focused file line
(defun move-line (n)
  "Move the current line up or down by N lines."
  (interactive "p")
  (setq col (current-column))
  (beginning-of-line) (setq start (point))
  (end-of-line) (forward-char) (setq end (point))
  (let ((line-text (delete-and-extract-region start end)))
    (forward-line n)
    (insert line-text)
    ;; restore point to original column in moved line
    (forward-line -1)
    (forward-char col)))

(defun move-line-up (n)
  "Move the current line up by N lines."
  (interactive "p")
  (move-line (if (null n) -1 (- n))))

(defun move-line-down (n)
  "Move the current line down by N lines."
  (interactive "p")
  (move-line (if (null n) 1 n)))

(global-set-key (kbd "M-<up>") 'move-line-up)
(global-set-key (kbd "M-<down>") 'move-line-down)
