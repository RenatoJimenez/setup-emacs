# README #

### What is this repository for? ###

This repository contains the emac preference files that include my personal preference of plugins and start up configs for emacs.

### How do I get set up? ###

    1) Store the repository files on your emacs load up folder ("list-directory ~/" emacs command). This directory is %appdata% on Win32 by default
    2) Launch emacs
    3) Done